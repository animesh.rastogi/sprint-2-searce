resource "google_compute_instance_template" "apache-template" {
  name = "apache-template"
  description = "This is the Instance-template for apache-webserver"

  machine_type = "e2-micro"
  can_ip_forward = false
  tags = [ "allow-http", "allow-https" ]
  disk {
    source_image = "ubuntu-os-cloud/ubuntu-1804-lts"
  }
  metadata_startup_script = file("scripts/startup.sh")
  network_interface {
    subnetwork = google_compute_subnetwork.subnet-3.name
    access_config {
      
    }
  }
}
resource "google_compute_region_instance_group_manager" "first-mig" {
  name = "first-mig"
  base_instance_name = "app"
  region = "us-central1"
  version {
    instance_template = google_compute_instance_template.apache-template.id
  }
}
resource "google_compute_region_autoscaler" "autoscaling" {
  region = "us-central1"
  name = "apache-autoscaler"
  target = google_compute_region_instance_group_manager.first-mig.self_link
  autoscaling_policy {
    min_replicas = 2
    max_replicas = 5
    cooldown_period = 60
    cpu_utilization {
        target = 0.7
    }
  }
}
terraform {
  backend "gcs"{
      bucket = "terraform-admin-54-files"
      prefix = "state-files"
  }
}