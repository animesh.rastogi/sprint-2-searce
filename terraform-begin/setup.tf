provider "google" {
    credentials = file("terraform-admin-54-b96d0223447e.json")
    project = "terraform-admin-54"
    region = "us-central1"
    zone = "us-central1-a"
}
resource "google_compute_network" "vpc_network" {
  name = "first-network"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnet-1" {
  name = "us-west1-subnet"
  ip_cidr_range = "10.2.0.0/16"
  region = "us-west1"
  network = google_compute_network.vpc_network.id
}
resource "google_compute_subnetwork" "subnet-2" {
  name = "us-east1-subnet"
  ip_cidr_range = "10.1.0.0/16"
  region = "us-east1"
  network = google_compute_network.vpc_network.id
  secondary_ip_range = [ {
    ip_cidr_range = "192.160.0.0/24"
    range_name = "us-east1-secondary-range"
  } ]
}
resource "google_compute_subnetwork" "subnet-3" {
  name = "us-central1-subnet"
  ip_cidr_range = "10.3.0.0/16"
  region = "us-central1"
  network = google_compute_network.vpc_network.id
}

resource "google_compute_firewall" "allow-access-ping" {
  name = "allow-web-and-ssh-access"
  network = google_compute_network.vpc_network.id
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports = ["80","8080", "443" ]
  }
  source_ranges = [ "0.0.0.0/0" ]
  target_tags = [ "allow-http", "allow-https" ]
}