## Description

This folder contains Kubernetes manifest files for some of the main Kubernetes objects like pods, services, configmaps, etc

## Ingress
This Ingress Resource provisions an External HTTPS Loadbalancer in GCP. It uses the Google Managed Certificate to enable TLS between Client and LB provisioned by managed-cert.yaml.

It is required to already have a simple deployment and a service which will be used as a backend for the Ingress.

## ConfigMap
To inject config details into your pods, use the following in the container section of the pod definition file:


```bash
envFrom:
-  configMapRef:
     name: config-map-name
```